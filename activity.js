//USERS

db.users.insertMany([
	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "85345548",
		email: "stephen.hawking@hr.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "HR"
	},
	{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "85345549",
		email: "neil.armstrong@hr.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "HR"
	},
	{
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "85345547",
		email: "jane.doe@hr.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "HR"
	}
])

/*
Find users with letter s in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field.

*/

db.users.find({
	$or: [
		{
			firstName: {
		$regex: "s",
		$options: "$i"
		}
		},

		{
			lastName: {
		$regex: "d",
		$options: "$i"
		}
		}
	]
},
		{
			firstName: 1,
			lastName: 1,
			_id: 0
		}
)

/*
Find users who are from the HR department and their age is greater
than or equal to 70.
a. Use the $and operator
*/

db.users.find({
	$and: [
		{
			department: "HR"
		},
		{
			age: {$gte: 70}
		},

	]
})


/*
Find users with the letter e in their first name and has an age of less
than or equal to 30.
a. Use the $and, $regex and $lte operators
*/

db.users.find({
	$and: [
		{
			firstName: {
			$regex: "e",
			$options: "$i"
			}
		},
		{
			age: {$lte: 30}
		},

	]
})












